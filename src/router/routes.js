
const routes = [

  { path: '', component: () => import('pages/IntroSlider.vue') },
  { path: '/login', component: () => import('pages/Login.vue'), name: 'login' },
  { path: '/home', component: () => import('pages/Home.vue'), name: 'home' },
  { path: '/sign_in', component: () => import('pages/SignIn.vue'), name: 'sign_in' },
  { path: '/forgot_pass', component: () => import('pages/ForgotPass.vue'), name: 'forgot_password' },
  { path: '/agendar', component: () => import('pages/Agendar.vue'), name: 'agendar' }

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
